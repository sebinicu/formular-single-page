import React from "react";
import "./App.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import FormCompanyDetails from "./components/FormCompanyDetails";
import LandingPage from "./components/LandingPage";
import CompanyForm from "./components/CompanyForm";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <div class="navbar"></div>
        <Switch>
          <Route exact path="/" component={LandingPage} />
          <Route path="/formular" component={CompanyForm} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
