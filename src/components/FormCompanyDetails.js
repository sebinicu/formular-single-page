import React, { Component } from "react";

export class FormCompanyDetails extends Component {
  continue = (e) => {
    e.preventDefault();
    this.props.nextStep();
  };

  render() {
    return (
      <div>
        <h1>Company Details</h1>
        <button className="btn btn-success" onClick={this.continue}>
          Next
        </button>
      </div>
    );
  }
}

export default FormCompanyDetails;
