import React, { Component } from "react";

export class FormAllCompaniesDetails extends Component {
  continue = (e) => {
    e.preventDefault();
    this.props.nextStep();
  };
  back = (e) => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    return (
      <div>
        <h1>All Companies Details</h1>
        <button onClick={this.back}>Back</button>
        <button onClick={this.continue}>Next</button>
      </div>
    );
  }
}

export default FormAllCompaniesDetails;
