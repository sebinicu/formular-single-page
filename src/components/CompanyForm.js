import React, { Component } from "react";
import FormCompanyDetails from "./FormCompanyDetails";
import FormPersonDetails from "./FormPersonDetails";
import FormAllCompanies from "./FormAllCompanies";
import Confirm from "./Confirm";
import Success from "./Success";

export class CompanyForm extends Component {
  state = {
    step: 1,
  };
  // next btn
  nextStep = () => {
    const { step } = this.state;
    this.setState({
      step: step + 1,
    });
  };
  //prev btn
  prevStep = () => {
    const { step } = this.state;
    this.setState({
      step: step - 1,
    });
  };

  render() {
    const { step } = this.state;

    switch (step) {
      case 1:
        return <FormCompanyDetails nextStep={this.nextStep} />;
      case 2:
        return (
          <FormPersonDetails
            nextStep={this.nextStep}
            prevStep={this.prevStep}
          />
        );
      case 3:
        return (
          <FormAllCompanies nextStep={this.nextStep} prevStep={this.prevStep} />
        );
      case 4:
        return <Confirm nextStep={this.nextStep} prevStep={this.prevStep} />;
      case 5:
        return <Success />;
    }
  }
}

export default CompanyForm;
