import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export class LandingPage extends Component {
  render() {
    return (
      <div>
        <NavLink to="/formular">Formular</NavLink>
        <h1>This is Landing Page</h1>
      </div>
    );
  }
}

export default LandingPage;
