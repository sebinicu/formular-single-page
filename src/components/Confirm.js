import React, { Component } from "react";

export class Confirm extends Component {
  continue = (e) => {
    e.preventDefault();
    this.props.nextStep();
  };
  back = (e) => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    return (
      <div>
        <h1>Confirm Page</h1>
        <button onClick={this.back}>Back</button>
        <button onClick={this.continue}>Next</button>
      </div>
    );
  }
}

export default Confirm;
